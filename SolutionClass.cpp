#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
#include<set>
#include <unordered_map>
using namespace std;

#include "SolutionClass.h"

SolutionClass::SolutionClass(graph Gin){
  myG = Gin;
  currentLabel = myG.size();
  currentLeader = 0;
  for (int i = 1; i <= currentLabel; i++)
 {
    myLabel[i] = i;
  }
}

void SolutionClass::ReverseGraph(graph Gin){
  myG = Gin;
}

void SolutionClass::DFS(int v){
  explored.insert(v);
  //cout<<"exploring "<<v<<endl;
  //cout<<"explored "<<explored.size()<<" nodes"<<endl;
  if (SCC.find(currentLeader) == SCC.end()){
    set<int> tempS;
    SCC[currentLeader] = tempS;
  }
  SCC[currentLeader].insert(v);
  for (set<int>::iterator setIt = myG[v].begin(); setIt != myG[v].end(); ++setIt){
    int u = *setIt;
    if (explored.find(u) == explored.end()){
      this->DFS(u);
    }
  }
  currentLabel ++;
  myLabel[currentLabel] = v;
  //cout<<"labeled "<<currentLabel<<" "<<v<<endl;
}

void SolutionClass::DFSLoop(){
  currentLabel = 0;
  currentLeader = 0;
  explored.clear();
  SCC.clear();
  label localLabel(myLabel);
  int n = myG.size();
  for (int i = n; i > 0; i --){
    int v = localLabel[i];
    //cout<<"i is "<<i<<endl;
    if (explored.find(v) == explored.end()){
      currentLeader = v;
      SolutionClass:DFS(v);
    }
  }
}