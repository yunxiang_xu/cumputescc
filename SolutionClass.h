#ifndef _SOLUTIONCLASS_H_
#define _SOLUTIONCLASS_H_

typedef unordered_map<int, set<int>> graph;
typedef   unordered_map<int, int> label;
class SolutionClass
{
private:
  graph myG;
  label myLabel;
  int currentLabel;
  int currentLeader;
  set<int> explored;
public:
  graph SCC;
  SolutionClass(graph Gin);
  void ReverseGraph(graph Gin);
  void DFS(int v);
  void DFSLoop();
};

#endif