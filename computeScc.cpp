#include<iostream>
#include<fstream>
#include<string>
#include<set>
#include <unordered_map>
#include<vector>
#include <algorithm>   
using namespace std;

typedef unordered_map<int, set<int>> graph;
typedef   unordered_map<int, int> label;

#include "ComputeSCC\SolutionClass.h"


int main()
{
  ios::sync_with_stdio(false);
  graph G, Gr;
  ifstream myF ("test.txt");
  int a, b;
  int i = 0;
  if (myF.is_open()){
    while (!myF.eof()){
      i ++;
      if (i % 987 == 123) cout<<"loading line "<<i<<" \r"<<flush;
      myF >> a;
      myF >> b;
      /*if (myF.eof()){
        cout<<"end of file"<<endl;
        break;
      }*/
      /*
      try{ G[a].insert(b);}
      catch(int e){
        set<int> temp;
        temp.insert(b);
        G[a] = temp;
      }
      if (G.find(b) == G.end()){
        set<int> temp;
        G[b] = temp;
      }
      try{ Gr[b].insert(a);}
      catch(int e){
        set<int> temp;
        temp.insert(a);
        Gr[b] = temp;
      }
      if (Gr.find(a) == Gr.end()){
        set<int> temp;
        Gr[a] = temp;
      }
      */
      if (G.find(a) == G.end()){
        set<int> tempS;
        G[a] = tempS;
      }
      G[a].insert(b);
      if (G.find(b) == G.end()){
        set<int> temp;
        G[b] = temp;
      }
      if (Gr.find(b) == Gr.end()){
        set<int> tempS;
        Gr[b] = tempS;
      }
      Gr[b].insert(a);
      if (Gr.find(a) == Gr.end()){
        set<int> temp;
        Gr[a] = temp;
      }
    }
  }
  myF.close();
  /*
  for (set<int>::iterator it = G[9].begin(); it!= G[9].end();it++){
    cout<<"9 out "<<*it<<endl;
  }*/
  cout<<"number of nodes is "<<G.size()<<endl;
  cout<<"total "<<i<<" lines"<<endl;
  SolutionClass sol(G);
  cout<<"First loop starting..."<<endl;
  sol.DFSLoop();
  sol.ReverseGraph(Gr);
  cout<<"Second loop starting..."<<endl;
  sol.DFSLoop();
  cout<<"total "<<sol.SCC.size()<<" SCCs"<<endl;
  vector<int> output (5,0);
  for (graph::iterator it = sol.SCC.begin(); it != sol.SCC.end(); ++it){
    if(output[0] < it->second.size()){
      output[0] = it->second.size();
      sort(output.begin(),output.end());
    }
  }
  for (int i = 0; i < 5; i++){
    cout<<"SCC size is "<<output[i]<<endl;
  }
  system("PAUSE");
  return 0;
}