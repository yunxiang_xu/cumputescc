import copy
import time
class solution:
    label = {}
    G ={}
    c_label = 0
    c_leader = 0
    SCC = {}
    explored = set()
    expStack = []
    def __init__(self, Gin):
        self.G = Gin
        self.c_label = len(Gin)
        for node in self.G:
            self.label[node] = node

    def ReverseG(self, Grin):
        self.G = Grin
        
    def DFS(self, v):
        self.expStack.append(v)
        self.explored.add(v)
        try: self.SCC[self.c_leader].add(v)
        except: self.SCC[self.c_leader] = set([v])
        while(self.expStack !=[]):
            currentNode = self.expStack[len(self.expStack)-1]
            try:
                endNode = self.G[currentNode].pop()
                if endNode in self.explored: continue
                else:
                    self.SCC[self.c_leader].add(endNode)
                    self.expStack.append(endNode)
                    self.explored.add(endNode)
            except:
                self.expStack.pop()
                self.c_label += 1
                self.label[self.c_label] = currentNode
    
    def DFS_Loop(self):
        self.c_label = 0
        self.c_leader = 0
        self.expStack = []
        self.explored = set()
        self.SCC = {}
        n =  len(self.G.keys())
        l_label = copy.deepcopy(self.label)
        for i in range(n,0,-1):
            v = l_label[i]
            
            if v not in self.explored:
                self.c_leader = v
                self.DFS(v)
                
t0 = time.time()
# load the graph and reversed graph
f = open('_410e934e6553ac56409b2cb7096a44aa_SCC.txt')
#f = open('test.txt')
G = {}
Gr = {}
print 'loading starting...'
i = 1
for line in f:
    #if i > 200000: break
    if i % 953 == 548: print '{0}\r'.format(i),
    edgeStr = line.split()
    edge = [int(node) for node in edgeStr]
    # add edge to G
    try:
        G[edge[0]].add(edge[1])
    except:
        G[edge[0]] = set()
        G[edge[0]].add(edge[1])
    if edge[1] not in G: G[edge[1]] = set()
        
    try:
        Gr[edge[1]].add(edge[0])
    except:
        Gr[edge[1]] = set()
        Gr[edge[1]].add(edge[0])
    if edge[0] not in Gr: Gr[edge[0]] = set()
    #G[edge[1]] = set()
    i+=1
f.close()
print 'time', time.time()-t0
print 'total nodes ', len(G)
print 'loading done'
sol = solution(G)
print 'first loop starting...'
sol.DFS_Loop()
print 'first loop done'
print 'time', time.time()-t0
sol.ReverseG(Gr)

print 'second loop starting...'
sol.DFS_Loop()
print 'second loop done'
print 'time', time.time()-t0
print 'total', len(sol.SCC),'SCCs'
output = [0]*5
for scc in sol.SCC.values():
    size = len(scc)
    if size > output[0]:
        output[0] = size
        output.sort()
#print sol.SCC
print output
    
#print len(G)
#print len(Gr)
#print '1 to',G[1]
#print '2 to',Gr[2]



